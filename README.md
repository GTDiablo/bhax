### Prog2 Fájlok
Ezene a path-en lehet megtalálni a prog2-es feladatokat:
```/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod```

Itt van a könyvnek a pdf-je is, ami minden commit előtt le lesz buildel-ve.
A - ***Forraskodok*** - mappába lesz minden EPAM-os vagy más java-ás forráskód.
Java forráskódok nem egy IDE-hez megszokott módon van rendszerezve, mert online java environment-et használok.
Minden class 1 fájlban van, de így is tökéletesen működi, minden feladathoz lesz ***repl.it*** -es link, amin lehet azonnal futtatni.

# BHAX

A BHAX csatorna forráskódjai. 
(Élő adások: https://www.twitch.tv/nbatfai, archívum: https://www.youtube.com/c/nbatfai)
A csatorna célja a szervezett számítógépes játék és a programozás népszerűsítése, tanítása, különös tekintettel a mesterséges intelligenciára! 
Hosszabb távon utánpótlás esport csapatok szervezésének tartalmi támogatása. 


### Feladatok száma
| Turing (9) | Gutenberg (4) | Chomsky (9) | Caesar (7) | Mandelbrot (7) | Welch (7) | Conway (5) | Schwarzenegger (4) | Chaitin (4) |
| :--------: | :-----------: | :---------: | :--------: | :------------: | :-------: | :--------: | :----------------: | :---------: |
|     9      |       4       |      9      |     7      |       7        |     7     |     5      |         4          |      4      |

### P1TP pontok
| Labor | Előadás |
| :---: | :-----: |
|  40   |   71    |

- előadás
  - Minecraft smart steve 21 rf & magyarázat
  - minecraft csiga -
  - minecraft mit lát steve -
  - minecraft láva oda vissza -
  - minecraft csiga folytonos -

- labor
    - XOR 
    - human inteligence
    - telefonos cucc
    - tarts rajta az egeret 10 percig cucc
    - bogomips

# ! Windowson van egy txt-ben miket csináltam meg, linuxon nem férek hozzá.... Majd frissítem
