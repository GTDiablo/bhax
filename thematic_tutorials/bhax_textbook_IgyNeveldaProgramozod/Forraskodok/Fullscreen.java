import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Fullscreen {
  public static void main(String[] args){
    GraphicsEnvironment graphics = GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice[] devices = graphics.getScreenDevices();

    GraphicsDevice device;
    if(devices.length > 1){
      device = devices[1];
    } else {
      device = devices[0];
    }

    System.out.println("Eszközök száma: " + devices.length);
    System.out.println("Ezt a monitort használjuk: " + device);

    JFrame frame = new JFrame("Fullscreen");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    JLabel label = new JLabel("Katt ide a kilépéshez", JLabel.CENTER);

    label.setForeground(Color.gray);
    label.setFont(new Font("Verdana", Font.PLAIN, 30));

    label.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent e)
      {
        frame.dispose();
      }
    });
    frame.add(label);
    frame.setUndecorated(true);
    frame.setBackground(new Color(242,238,203));

  }
}