
//A szülő class
class Szulo {
    int a = 0;

    //Egy függvény (nem kell virtuálisnak állítani)
    //Ezek szerint mindig dinamikus a kötés
    String nev()
    {
        return "Szulo";
    }

    static void test()
    {
        Szulo szulo = new Szulo();  //csinálunk egy új szülőt és egy szülő típusú változóhoz kötjük
        Szulo gyerek = new Gyerek();    //csinálunk egy új gyereket és egy szülő típusú változóhoz kötjük

        System.out.println(szulo.nev());
        System.out.println(gyerek.nev());   //a gyerek által módosított függvény fut le
        //System.out.println(gyerek.kor()); //hibát ad, hiszen a szülőnek nincs ilyen metódusa, csak a gyereknek
    }
}


class Gyerek extends Szulo {
    @Override
    String nev() {
        return "Gyerek";
    }

    int kor()
    {
        return 5;
    }
}

class Main {
  public static void main(String[] args) {
    Gyerek gy = new Gyerek();
    Szulo sz = new Gyerek();
  }
}