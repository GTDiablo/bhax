#include <string>
#include <iostream>

class Szulo
{
public:
	int a = 5;

	//elore meg kell adni hogy virtual lesz a fggvny -> alapbl statikusan van ktve
	virtual std::string nev()
	{
		return "Szulo";
	}	
};

class Gyerek : Szulo
{
	//nem kell megmondani hogy overridolunk, azt kitallja a compiler
	//ha egy nem virtulis funkcival megegyezo nevu metdust csinlunk, 
	//akkor shadowolni fogja (elrejti) az eredetit, s azt csak akkor lehet elrni, ha castolunk
	std::string nev()
	{
		return "Gyerek";
	} 
};

static void test()
{
	Szulo* szulo = new Szulo();
	Szulo* gyerek = (Szulo*) new Gyerek();	//explicit castolni kell, nem dinamikus

	Szulo& gyerek2 = *((Szulo*) new Gyerek());	//itt is

	std::cout << szulo->nev() << gyerek->nev() << gyerek2.nev();
}
