enum Sex {
  MALE,
  FEMALE
}

class Person {
  private String name;
  private int age;
  private Sex sex;

  public static String getSexString(Sex sex){
    switch(sex){
      case MALE:
        return "Male";
      case FEMALE:
        return "Female";
    }
    return ""; // Ez muszáj ide, mert nem szeret a java :/
  }

  public Person(String name, int age, Sex sex){
    this.name = name;
    this.age = age;
    this.sex = sex;
  }

  public String toString(){
    return String.format(
      "Person(%s, %d, %s)",
      this.name,
      this.age,
      Person.getSexString(this.sex)
    );
  }

  public Person closne(){
    return new Person(this.name, this.age, this.sex);
  }
}

class Main {
  public static void main(String[] args) {
    Person zsolt = new Person("Zsolt", 20, Sex.MALE);
    Person clone = zsolt.closne();

    System.out.println(zsolt);
    System.out.println(clone);
  }
}