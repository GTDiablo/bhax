class Websocket {
  private static Websocket instance = null;

  public static Websocket getInstance(){
    if(instance == null)
      instance = new Websocket();
    return instance;
  }

  public String toString(){
    return String.format("Websocket(hashCode: %s)", this.hashCode());
  }
}

class Main {
  public static void main(String[] args) {
    // Ugyanazt a globális 'instance'-t kérem le
    Websocket ws1 = Websocket.getInstance();
    Websocket ws2 = Websocket.getInstance();

    // Mind a 2 változó ugyanarra a 'instance'-re mutat.
    System.out.println(ws1);
    System.out.println(ws2);
  }
}