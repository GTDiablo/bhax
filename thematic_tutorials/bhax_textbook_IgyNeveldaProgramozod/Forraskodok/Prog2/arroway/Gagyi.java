package com.company;

public class Gagyi {
    public static void gagyi()
    {
        NInteger a = NInteger.valueOf(5);
        NInteger b = NInteger.valueOf(5);

        System.out.println("a: "+a);
        System.out.println("b: "+b);

        System.out.println("a <= b: "+a.smallerI(b));
        System.out.println("a >= b: "+a.largerI(b));
        System.out.println("a == b: "+(b == a)+"\n");


        a = NInteger.valueOf(129);
        b = NInteger.valueOf(129);

        System.out.println("a: "+a);
        System.out.println("b: "+b);

        System.out.println("a <= b: "+a.smallerI(b));
        System.out.println("a >= b: "+a.largerI(b));
        System.out.println("a == b: "+(b == a));
        System.out.println("a.equals(b): "+a.equals(b));

    }


}

class NInteger
{
    int value;

    NInteger(int i)
    {
        value = i;
    };

    public static NInteger valueOf(int i)
    {
        final int offset = 128;
        if (i >= -128 && i <= 127) // must cache
            return NIntegerCache.cache[i + offset];
        return new NInteger(i);
    }

    private static class NIntegerCache
    {
        private NIntegerCache(){}
        static final NInteger cache[] = new NInteger[-(-128) + 127 + 1];
        static {
            for(int i = 0; i < cache.length; i++)
                cache[i] = new NInteger(i - 128);
        }
    }

    //returns 0 if equal, + if larger, - if smaller
    public int compare(NInteger other)
    {
        return value-other.value;
    }

    public boolean larger(NInteger other)
    {
        return this.compare(other) > 0;
    }

    public boolean smaller(NInteger other)
    {
        return this.compare(other) < 0;
    }

    public boolean largerI(NInteger other)
    {
        return this.compare(other) >= 0;
    }

    public boolean smallerI(NInteger other)
    {
        return this.compare(other) <= 0;
    }

    public boolean equal(NInteger other)
    {
        return this.compare(other) == 0;
    }

    @Override
    public String toString() {
        return Integer.toString(value);
    }

    @Override
    public boolean equals(Object obj) {
        NInteger other = (NInteger) obj;
        if (other == null)
            return false;
        else
            return other.value == this.value;
    }
}