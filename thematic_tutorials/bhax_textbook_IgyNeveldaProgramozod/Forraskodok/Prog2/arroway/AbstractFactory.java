interface Pet {
  public void speak();
}

class Dog implements Pet {
  public void speak(){
    System.out.println("Vau-Vau");
  }
}

class Cat implements Pet {
  public void speak(){
    System.out.println("Miau-Miau");
  }
}

interface AbstractPetFactory {
  public Pet getPet();
}

class DogFactory implements AbstractPetFactory{
  public DogFactory(){

  }

  public Pet getPet(){
    return new Dog();
  }
}

class CatFactory implements AbstractPetFactory{
  public CatFactory(){

  }

  public Pet getPet(){
    return new Cat();
  }
}

class PetFactory {
  public static Pet getPet(AbstractPetFactory factory){
    return factory.getPet();
  }
}


class Main {
  public static void main(String[] args) {
    Pet my_dog = PetFactory.getPet(new DogFactory());
    Pet my_cat = PetFactory.getPet(new CatFactory());

    my_dog.speak();
    my_cat.speak();
  }
}