class Person {
  private String name;
  private int age;

  public Person(String name, int age){
    this.name = name;
    this.age = age;
  }

  public String getName(){return this.name;}
  public int getAge(){return this.age;}

  @Override
  public String toString(){
    return String.format("Person(name: %s, age: %d, hash: %d)", this.name, this.age, this.hashCode());
  }

  @Override
  public int hashCode(){
    return this.age * 20 % 3;
  }

  @Override
  public Person clone(){
    return new Person(this.name, this.age);
  }

  public boolean equals(Person obj){
    if(obj instanceof Person){
      return this.name == obj.name && this.age == obj.age;
    }
    return false;
  }

  @Override
  protected void finalize(){
    System.out.println(String.format("%s went into the garbage.", this.name));
  }

}

class Main {
  public static void main(String[] args) {
    Person zsolt = new Person("Zsolt", 20);
    Person niki = new Person("Niki", 19);
    Person clone = zsolt.clone();

    System.out.println(zsolt);
    System.out.println(niki);
    System.out.println(clone);

    if(zsolt.equals(clone))
      System.out.println("Zsolt and Clone are equals!");

    System.out.println(zsolt.getClass());

    zsolt.finalize();

  }
}