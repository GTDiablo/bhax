class Robot {
  private String name;
  private String left_arm;
  private String right_arm;
  private String processor;

  public Robot(String name){
    this.name = name;
  }

  
  public String toString(){return String.format("Robot(%s)", this.name);}

  public Robot addLeftArm(String arm){
    this.left_arm = arm;
    return this;
  }

  public Robot addRightArm(String arm){
    this.right_arm = arm;
    return this;
  }

  public Robot addProcessor(String proc){
    this.processor = proc;
    return this;
  }

  public boolean hasProcessor(){return this.processor != null;}
  public boolean hasLeftArm(){return this.left_arm != null;}
  public boolean hasRightArm(){return this.right_arm != null;}

}

class Main {
  public static void main(String[] args) {
    Robot robot = new Robot("MyRobot");
    System.out.println(robot);

    if(robot.hasProcessor())
      System.out.println("MyRobot has a processor");
    else
      System.out.println("MyRobot doesnt have a processor");

    robot
    .addLeftArm("Left_Arm_Unit")
    .addProcessor("Intel Pentium 1 Core IDK");

    if(robot.hasProcessor())
      System.out.println("MyRobot has a processor");
    else
      System.out.println("MyRobot doesnt have a processor");

    
  }
}