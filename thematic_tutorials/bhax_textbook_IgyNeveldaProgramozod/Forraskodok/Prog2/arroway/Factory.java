enum EnemyType {
  BOSS,
  MINION
}

class Enemy{
  private EnemyType _type;
  private String _name;
  private int _health;
  private int _damage;

  public static String getEnemyTypeString(EnemyType type){
    switch(type){
      case BOSS:
        return "Boss";
      case MINION:
        return "Minion";
      default:
        return "Enemy";
    }
  }

  // Factory metódus
  public static Enemy get(EnemyType type){
    switch(type){
      case BOSS:
        return new Boss();
      case MINION:
        return new Minion();
    }
    return null;
  }

  public Enemy(EnemyType type, String name, int hp, int dmg){
    this._type = type;
    this._name = name;
    this._health = hp;
    this._damage = dmg;
  }

  public String toString(){
    return String.format("%s(%s, hp:%d, dmg:%d)",
        Enemy.getEnemyTypeString(this._type),
        this._name,
        this._health,
        this._damage
      );
  }

  public void takeDamage(int damage){ this._health -= damage; }
  public void attack(Enemy other){ other.takeDamage(this._damage);}

}

class Boss extends Enemy{
  public Boss(){
    super(EnemyType.BOSS, "BigBoss", 100, 100);
  }
}

class Minion extends Enemy{
  public Minion(){
    super(EnemyType.MINION, "LittleGuy", 10, 10);
  }
}

// Külön Factory class 
class EnemyFactory{
  public static Enemy get(EnemyType type){
    switch(type){
      case BOSS:
        return new Boss();
      case MINION:
        return new Minion();
    }
    return null;
  }
}

class Main {
  public static void main(String[] args) {
    Enemy b = Enemy.get(EnemyType.BOSS);
    Enemy m = Enemy.get(EnemyType.MINION);

    System.out.println(b);
    System.out.println(m);

    m.attack(b);

    System.out.println(b);

  }
}